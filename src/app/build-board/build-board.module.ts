import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuildBoardService } from './build-board.service';
import { BuildBoardComponent } from './build-board.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';



@NgModule({
  declarations: [BuildBoardComponent],
  imports: [
    CommonModule,
    MatSelectModule,
    MatFormFieldModule,
    MatButtonModule
  ],
  providers: [
    BuildBoardService
  ],
  exports:[
    BuildBoardComponent
  ]
})
export class BuildBoardModule { }
