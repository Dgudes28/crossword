import { BuildBoardService } from './build-board.service';
import { OnInit, Component } from '@angular/core';

@Component({
  selector: 'app-build-board',
  templateUrl: './build-board.component.html',
  styleUrls: ['./build-board.component.less']
})
export class BuildBoardComponent implements OnInit {
  sizeOptions: number[] = [];

  constructor(private buildBoardService: BuildBoardService) { 
  }
  ngOnInit() {
    this.sizeOptions = this.buildBoardService.buildArrayOfNumbersFromOneToLength(9);
  }
  public buildBoard():void {
    //build the board for setting the crossward
  }

}
