import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BuildBoardService {

  numberOfRows: number[] = [];
  numberOfColumns: number[] = [];
  constructor() {
  }

  public buildArrayOfNumbersFromOneToLength(length: number): number[] {
    let numbersArray: number[] = [];
    for (let num = 1; num <= length; num++) {
      numbersArray.push(num);
    }
    debugger;
    return numbersArray;
  }
}
