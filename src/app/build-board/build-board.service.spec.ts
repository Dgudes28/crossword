import { TestBed } from '@angular/core/testing';

import { BuildBoardService } from './build-board.service';

describe('BuildBoardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BuildBoardService = TestBed.get(BuildBoardService);
    expect(service).toBeTruthy();
  });
});
