import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.less']
})
export class BoardComponent implements OnInit {

  numberOfInputsInRow: number[] = [];
  numberOfInputsInCol: number[] = [];
  constructor() { }

  ngOnInit() {
    for (let i = 0; i <= 9; i++) {
      this.numberOfInputsInRow.push(i);
      this.numberOfInputsInCol.push(i);
    }
  }

}
